/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biometriconotarios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author ralpuchev
 */
public class WSClient {

    private static String POST_METHOD = "POST";
    private String Method;
    private String bodyParams;
    private String Type;
    private String WSUri = "http://www.colegiodenotariosmichoacan.org.mx/intranet/dentro/huella/notarios/ms/EnlaceNotarios.php";
    public static String WSWeb = "http://www.colegiodenotariosmichoacan.org.mx/intranet/dentro/huella/notarios/";

    public String getMethod() {
        return Method;
    }

    public void setMethod(String Method) {
        this.Method = Method;
    }

    public String getBodyParams() {
        return bodyParams;
    }

    public void setBodyParams(String bodyParams) {
        this.bodyParams = bodyParams;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }
    
    
    public WSClient(){
        this.Method = "";
        this.Type = "";
        this.bodyParams = "";
    }
    
    public WSClient(String Method, String bodyParams, String Type) {
        this.Method = Method;
        this.bodyParams = bodyParams;
        this.Type=Type;
    }
    
    public String WSCall() throws MalformedURLException, IOException{
        String response="";
        URL url = new URL(WSUri + getMethod());
        HttpURLConnection client = (HttpURLConnection) url.openConnection();
        
        client.setRequestMethod(getType());
        if(getType().equals(POST_METHOD)){
            client.setDoOutput(true);
            client.setRequestProperty("Content-Type","application/json");
            client.setRequestProperty("Cache-Control", "no-cache");

            client.setConnectTimeout(30*1000);
            OutputStream os = client.getOutputStream();
            os.write(getBodyParams().getBytes("UTF-8"));
            os.close();
            os.flush();    
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((client.getInputStream()),"UTF-8"));
        String output;
	while ((output = br.readLine()) != null) {
            response=output;
	}
        return response;
    }  
}
