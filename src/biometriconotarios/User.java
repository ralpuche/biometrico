/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biometriconotarios;

/**
 *
 * @author ralpuchev
 */
public class User {
    
    //token|idusuario|nombrecompleto|nombreusuario|permisos
    private String token;
    private String idUsuario;
    private String nombreCompleto;
    private String permisos;

    public User(String token, String idUsuario, String nombreCompleto, String permisos) {
        this.token = token;
        this.idUsuario = idUsuario;
        this.nombreCompleto = nombreCompleto;
        this.permisos = permisos;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getPermisos() {
        return permisos;
    }

    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }
}
